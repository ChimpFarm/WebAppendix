d3.csv("./data/devices.csv", function(error, data) {
	for (var i = 0; i<data.length; i++) {
		var tr = document.createElement("tr");
        var id = document.createElement("td");
        id.appendChild(document.createTextNode(i+1));
        tr.appendChild(id);
        var osv = document.createElement("td");
        osv.appendChild(document.createTextNode(data[i]["osv"]));
        tr.appendChild(osv);
        var sr = document.createElement("td");
        sr.appendChild(document.createTextNode(data[i]["sr"]));
        tr.appendChild(sr);
        var status = document.createElement("td");
        var statusSpan = document.createElement("span");
        if(data[i]["st"]==0){
        	statusSpan.className = "badge onbuild-badge";
        	statusSpan.appendChild(document.createTextNode("On Build"));
        }
        else if(data[i]["st"]==1){
        	statusSpan.className = "badge ontesting-badge";
        	statusSpan.appendChild(document.createTextNode("On Testing"));
        }
        else {
        	statusSpan.className = "badge available-badge";
        	statusSpan.appendChild(document.createTextNode("Available"));
        }
        status.appendChild(statusSpan);
        tr.appendChild(status);
        var rl = document.createElement("td");
        var rLink = document.createElement("a");
        rLink.setAttribute("href",data[i]["rl"]);
        rLink.className = "btn btn-md btn-primary";
        rLink.appendChild(document.createTextNode("Visit repository"));
        if(data[i]["st"]==0){
        	rLink.className = rLink.className+" disabled";
        }
        rl.appendChild(rLink);
        tr.appendChild(rl);
        if(data[i]["st"]==2){
        	$("#tableBody").append(tr);
        }
	};
})